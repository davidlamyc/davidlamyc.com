import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <p>I'm David, a software developer from Singapore.</p>
        <p>This site is currently used for testing deployment pipelines.</p>
        </header>
    </div>
  );
}

export default App;
